var image = {
        id: null,
        deleteToken: null,
    },
    task = {
        id: null,
        deleteToken: null,
    },
    result = {};

$('#upload button').click(function(event) {
    event.preventDefault();

    var formData = new FormData();
    formData.append('image', $('#upload :file')[0].files[0]);

    $.ajax({
        url: '/api/v1/images',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
    }).done(function(data) {
        image = {
            id: data.id,
            deleteToken: data.delete_token,
        };
        console.log(image);
        $('#steps a[href="#format"]').tab('show');
    }).fail(function(err) {
        console.log(err);
    });
});

$('#format button').click(function(event) {
    event.preventDefault();

    var outputFormat = $(this).data('output-format');

    // show or hide MIDI player
    $('#player').toggle(outputFormat === 'mid');

    $.ajax({
        url: '/api/v1/queue',
        type: 'POST',
        data: {
            image: image.id,
            output_format: outputFormat,
        },
    }).done(function(data) {
        task = {
            id: data.task_id,
            deleteToken: data.delete_token,
        };
        console.log(task);
        $('#process .status').text('Task was posted to queue.');
        $('#steps a[href="#process"]').tab('show');
        setTimeout(waitForTask, 2000);
    }).fail(function(err) {
        console.log(err);
    });
});

function waitForTask() {
    $.ajax({
        url: '/api/v1/queue/' + task.id,
        type: 'GET',
    }).done(function(data) {
        console.log(data);

        switch (data.status) {
            case 'done':
                // delete finished task
                $.ajax({
                    url: '/api/v1/queue/' + task.id,
                    type: 'DELETE',
                    data: {
                        delete_token: task.deleteToken,
                    }
                });

                // store result path
                result = {
                    path: data.result,
                };

                $('#process .status').text('No active task.');
                $('#result_url').attr('href', result.path);
                $('#steps a[href="#result"]').tab('show');
                return;

            case 'refused':
                $('#process .status').text('Your task was refused. Please try again.');
                return;

            default:
                $('#process .status').text('Your task is being processed...');

                // retry if still processed
                if (data.status !== 'done' && data.status !== 'refused')
                    setTimeout(waitForTask, 2000);
        }
    }).fail(function(err) {
        console.log(err);
    });
}

$('#play_midi').click(function(event) {
  event.preventDefault();
  MIDIjs.play(result.path);
});

$('#stop_midi').click(function(event) {
  event.preventDefault();
  MIDIjs.stop();
});
