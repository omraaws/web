import hashlib
import logging
import os
import shutil
import uuid

from bson.objectid import ObjectId
from flask import Flask, request, redirect, send_from_directory, render_template
from flask_pymongo import PyMongo
from flask_restful import Resource, Api, reqparse, abort
from mimetypes import guess_extension
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

class Format:
    PDF = 'pdf'
    XML = 'xml'
    MIDI = 'mid'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = os.environ.get('UPLOAD_FOLDER', '/data/images')

api = Api(app, prefix='/api/v1')

app.config['MONGO_HOST'] = os.environ.get('MONGO_HOST', 'mongo')
mongo = PyMongo(app)

@app.before_first_request
def setup_logging():
    # add log handler to sys.stderr
    app.logger.addHandler(logging.StreamHandler())
    app.logger.setLevel(logging.DEBUG if app.debug else logging.INFO)

@app.route('/', methods=['GET', 'POST'])
def index():
    """Serve frontend page."""
    return render_template('index.html')

@app.route('/debug', methods=['GET', 'POST'])
def debug():
    """Serve debug page."""
    return render_template('debug.html')

class ImageList(Resource):
    def post(self):
        """Upload new image to storage."""
        # handle incoming image:
        parser = reqparse.RequestParser()
        parser.add_argument('image', required=True, type=FileStorage, location='files')
        args = parser.parse_args()

        # calculate sha1 hash
        original_filename = secure_filename(args.image.filename)
        image_hash = hashlib.sha1(args.image.read()).hexdigest()
        args.image.seek(0)  # rewind file

        # check allowed MIME type
        mimetype = args.image.mimetype
        is_correct_mime = mimetype.startswith('image/') or mimetype == 'application/pdf'
        if not is_correct_mime:
            app.logger.info("Got image with wrong MIME type: '%s' (hash: %s, mimetype: %s)",
                original_filename, image_hash, args.image.mimetype)
            abort(400, message={'image': "Incorrect image format"})

        # lookup in mongo
        image_info = mongo.db.images.find_one({'_id': image_hash})
        if image_info is not None:
            app.logger.info("Discarding already uploaded image '%s' (hash: %s)",
                original_filename, image_hash)
            return {
                'id': image_hash,
            }

        # get image info
        extension = guess_extension(args.image.mimetype)
        target_filename = 'image' + extension
        directory = os.path.join(app.config['UPLOAD_FOLDER'], image_hash)
        target_path = os.path.join(directory, target_filename)

        # store to disk
        os.makedirs(directory, exist_ok=True)
        args.image.save(target_path)
        saved_size = os.path.getsize(target_path)
        if saved_size == 0:
            app.logger.info("Got empty image '%s' (hash: %s, mimetype: %s)",
                original_filename, image_hash, args.image.mimetype)
            shutil.rmtree(directory, ignore_errors=True)
            abort(400, message={'image': "Empty file cannot be processed"})
        app.logger.info("Stored incoming image '%s' (hash: %s, mimetype: %s, size: %s) as '%s'",
            original_filename, image_hash, args.image.mimetype, saved_size, target_path)

        # assign random delete token
        delete_token = uuid.uuid4().hex

        # insert into mongo db
        mongo.db.images.insert_one({
            '_id': image_hash,
            'original_filename': original_filename,
            'mimetype': args.image.mimetype,
            'guessed_extension': extension,
            'target_directory': directory,
            'target_filename': target_filename,
            'target_path': target_path,
            'delete_token': delete_token,
        })

        return {
            'id': image_hash,
            'delete_token': delete_token,
        }

class Image(Resource):
    def delete(self, image_hash):
        """Delete image with related artifacts."""
        parser = reqparse.RequestParser()
        parser.add_argument('delete_token', required=True)
        args = parser.parse_args()

        # lookup in mongo
        image_info = mongo.db.images.find_one_or_404({'_id': image_hash})
        if args.delete_token != image_info['delete_token']:
            abort(403, message={'delete_token': "Incorrect delete token for %s" % image_hash})

        # remove from database
        mongo.db.artifacts.remove({'image': image_hash})
        mongo.db.images.remove({'_id': image_hash})

        # unlink from storage
        # this will erase original image & all related artifacts
        shutil.rmtree(image_info['target_directory'], ignore_errors=True)

        app.logger.info("Deleted data associated with image %s", image_hash)
        return None, 204

class Artifact(Resource):
    def get(self, image_hash, output_format):
        """Send artifact to the user."""
        # lookup in mongo
        artifact_id = "{image}.{format}".format(image=image_hash, format=output_format)
        artifact = mongo.db.artifacts.find_one_or_404({'_id': artifact_id})
        app.logger.debug("Serving artifact %s", artifact)
        return send_from_directory(artifact['directory'], artifact['filename'],
            as_attachment=True, attachment_filename=artifact_id)

class QueuePost(Resource):
    def post(self):
        """Store task to queue."""
        parser = reqparse.RequestParser()
        parser.add_argument('image', required=True)
        parser.add_argument('output_format', choices=[Format.XML, Format.PDF, Format.MIDI], required=True)
        args = parser.parse_args()

        # assign random delete token
        delete_token = uuid.uuid4().hex

        # prepare task for insertion
        task = {
            'image': args.image,
            'status': 'new',
            'output_format': args.output_format,
            'delete_token': delete_token,
        }

        # store task to queue
        result = mongo.db.queue.insert_one(task)
        app.logger.info("Stored new task to queue: %s", task)

        return {
            'task_id': str(result.inserted_id),
            'delete_token': delete_token,
        }

class Queue(Resource):
    def get(self, task_id):
        """Return queue task information."""
        task = mongo.db.queue.find_one_or_404({'_id': ObjectId(task_id)})

        result = api.url_for(Artifact, image_hash=task['image'], output_format=task['output_format']) \
                 if task['status'] == 'done' else None

        return {
            'task_id': str(task['_id']),
            'image': task['image'],
            'output_format': task['output_format'],
            'status': task['status'],
            'result': result,
        }

    def delete(self, task_id):
        """Delete queue task."""
        parser = reqparse.RequestParser()
        parser.add_argument('delete_token', required=True)
        args = parser.parse_args()

        # lookup in mongo
        task_info = mongo.db.queue.find_one_or_404({'_id': ObjectId(task_id)})
        if args.delete_token != task_info['delete_token']:
            abort(403, message={'delete_token': "Incorrect delete token for %s" % task_id})

        # remove from database
        mongo.db.queue.remove({'_id': ObjectId(task_id)})

        app.logger.info("Deleted data associated with task %s", task_id)
        return None, 204

api.add_resource(ImageList, '/images', endpoint='images')
api.add_resource(Image, '/image/<image_hash>', endpoint='image')
api.add_resource(Artifact, '/artifact/<image_hash>/<output_format>', endpoint='artifact')
api.add_resource(QueuePost, '/queue', endpoint='queue')
api.add_resource(Queue, '/queue/<task_id>', endpoint='task')

if __name__ == '__main__':
    host = os.environ.get("HOST", '0.0.0.0')
    port = int(os.environ.get("PORT", 8000))
    app.run(host=host, port=port, debug=True)
